<#
.SYNOPSIS
Install-CCSQLServer script installs SQL server quietly.
.DESCRIPTION
Install-CCSQLServer script performs installation of SQL Server Express edition. Make shure path of
installation exe is reachable from remote machine on which unattended installation is performed.
.PARAMETER Path
Path to installation executable, which can be local path on machine or UNC path.
.PARAMETER ComputerName
Name of remote computer if not defined defaults to localhost.
.PARAMETER SAAccount
SQL Server Sys Admin account name.
.EXAMPLE
Install SQL Server from UNC path on remote computer PC-01 and set SA account to local
user john.smith:
Install-CCSQLServer -Path \\network\share\sqlSetupDir\setup.exe -ComputerName 'PC-01' `
    -SAAccount 'PC-01\john.smith'
.EXAMPLE
Install SQL Server from local path on remote computer PC-02 and set default SA account to local
account sqladmin, which is member of Users group.
Install-CCSQLServer -Path 'd:\SQLServer\setup.exe' -ComputerName 'PC-02'
.NOTES
AUTHOR: Bojan G. Kalicanin
DATE: 31-Jul-2018
VERSION: 1.00
#>
[CmdletBinding()]
param(
    [Parameter(Mandatory=$true,HelpMessage='Enter remote computer name')]
    [Alias('Hostname')]
    [string[]]$ComputerName='localhost',
    [Parameter(Mandatory=$true,HelpMessage='Enter path to installation executable')]
    [string]$Path,
    [Parameter(Mandatory=$true,HelpMessage='Enter SA account user name')]
    [string]$SAAccount
)

$quietInstallArgs = "/ACTION=Install /Q /INSTANCENAME=SQLEXPRESS /IACCEPTSQLSERVERLICENSETERMS=1 `
    /FEATURES=SQLENGINE,FullText,AS,RS,IS,BC,Conn,SNAC_SDK,SDK,SSMS `
    AGTSVCACCOUNT=`"NT AUTHORITY\NETWORK SERVICE`" AGTSVCSTARTUPTYPE=`"Enabled`" `
    /INDICATEPROGRESS /SQLSYSADMINACCOUNTS=`"$SAAccount`" /ENU"
$defaultSAUser = 'sqladmin'

Invoke-Command -ComputerName $ComputerName -ArgumentList $Path,$quietInstallArgs,$defaultSAUser `
    -ScriptBlock {
        param($path,$args,$defaultSA)
        # Create new user account and add it to local Users group if SA account not supplied
        if (!$SAAccount) {
            $securePass = Read-Host -Prompt "Enter SA password" -AsSecureString
            New-LocalUser -Name "$defaultSA" -Description "SQL Server admin account" -AccountNeverExpires `
                -Password $securePass -PasswordNeverExpires
            Add-LocalGroupMember -Group Users -Member $defaultSA
            $SAAccount = "$env:COMPUTERNAME\$defaultSA"
    }
    
    & $path ($args)
}
