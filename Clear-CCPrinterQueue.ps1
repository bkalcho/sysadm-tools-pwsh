﻿<#
.SYNOPSIS
Clear-CCPrintQueue cmdlet clears print queue.
.DESCRIPTION
Clear-CCPrintQueue cmdlet clears print queue from one or more computers.
.PARAMETER ComputerName
Defines the name of target computer.
.PARAMETER ServiceName
Defines the name of service which is holding queue.
.PARAMETER Credential
Defines the credentials object.
.EXAMPLE
Clear the print queue when there are stuck jobs. First credentials object is created. This is not
mandatory as it will be automatically created upon user prompt if not supplied:

$cred = Get-Credential
Clear-CCPrintQueue -ComputerName SERVER-R01 -ServiceName Spooler `
	-Path 'C:\WINDOWS\System32\Spool\PRINTERS -Credential $cred
.NOTES
AUTHOR: Bojan G. Kalicanin
DATE: 07-Aug-2018
VERSION: 1.00
#>
[CmdletBinding()]
param(
	[Parameter(Mandatory=$true,HelpMessage='Enter print server computer name')]
	[Alias('Hostname')]
	[string[]]$ComputerName,
	[Parameter(HelpMessage='Enter service name')]
	[string]$ServiceName = 'Spooler',
	[Parameter(HelpMessage='Enter location path')]
	[Alias('PrintQueueLocation')]
	[string]$Path = "C:\Windows\System32\Spool\PRINTERS",
	[System.Management.Automation.PSCredential]$Credential = (Get-Credential),
	[ValidateSet('Continue','SilentlyContinue','Inquire','Stop')]
	$VerbosePreference = 'Continue'
)

function Clear-PrintQueue {
	param(
		[Parameter(Mandatory=$true)]
		[string[]]$ComputerName,
		[Parameter(Mandatory=$true)]
		[string]$ServiceName,
		[Parameter(Mandatory=$true)]
		[string]$Path,
		[Parameter(Mandatory=$true)]
		[System.Management.Automation.PSCredential]$Credential
	)

	foreach ($computer in $ComputerName) {
		Invoke-Command -ComputerName $computer -ArgumentList $ServiceName,$Path -Credential $Credential -ScriptBlock {
			param($srvName,$location)
			$VerbosePreference=$USING:VerbosePreference
			$srvStatus = (Get-Service -Name $srvName).Status
			while ($srvStatus -eq 'Running') {
				Write-Verbose "Stopping spooler service..."
				Stop-Service -Name $srvName
				$srvStatus = (Get-Service -Name $srvName).Status	
			}
			Write-Verbose "Service `"$srvName`" stopped."
			Write-Verbose "Started cleaning stuck jobs in `"$location`" location..."
			Remove-Item -Path "$location\*.*" -Verbose -Force
			Write-Verbose "Staring service `"$srvName`"..."
			Start-Service -Name $srvName
			Write-Verbose "Service started, finishing task execution..."
		}
	}
}

Clear-PrintQueue -ComputerName $ComputerName -ServiceName $ServiceName -Path $Path `
	-Credential $Credential