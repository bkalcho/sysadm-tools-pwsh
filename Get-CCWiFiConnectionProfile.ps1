<#
.SYNOPSIS
Get-CCWiFiConnectionProfile check if wifi connection profile exists.
.DESCRIPTION
.PARAMETER ComputerName
.PARAMETER WiFiProfile
.EXAMPLE
.NOTES
AUTHOR: Bojan G. Kalicanin
DATE: 27-Jul-2018
VERSION: 1.00
TODO: Implement function for automatic creation of desired WPA2 Enterprise
profile if it does not exists.
#>
[CmdletBinding()]
param(
    [Parameter(Mandatory=$true,HelpMessage='Enter remote computer name')]
    [Alias('Hostname')]
    [string[]]$ComputerName,

    [Parameter(Mandatory=$true,HelpMessage='Enter WiFi profile name')]
    [Alias('SSID')]
    [string]$WiFiProfile
)

$f = {
    function Get-WiFiConnectionProfile {
        <#
        Return true if WiFi profile connection profile
        has been found. Otherwise return false.
        #>
        param(
            [Parameter(Mandatory=$true,HelpMessage='Enter WiFi profile name')]
            [Alias('SSID')]
            $ProfileName
        )

        $connectionProfileName = (netsh wlan show profiles | Select-String -Pattern ".*$ProfileName.*" | Select-Object -Property line).Line.Trim()
        if ($connectionProfileName) {
            $connectionProfileStatus = $true
        } else {
            $connectionProfileStatus = $false
        }

        return $connectionProfileStatus
    }
}

$result = Invoke-Command -ComputerName $ComputerName -ArgumentList $f,$WiFiProfile -ScriptBlock {
    param($f,$WiFiProfile)
    Invoke-Expression $f
    Get-WiFiConnectionProfile -ProfileName $WiFiProfile
}
$result