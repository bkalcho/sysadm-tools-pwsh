<#
.SYNOPSIS
Get-CCAttachedMonitor retrieves locally attached monitors.
.DESCRIPTION
Get-CCAttachedMonitor retrieves detetcted monitors attached to the workstation.
It includes also the built-in monitor of the laptop or All-in-One computer and
all other monitors. It will list only active (turned-on) monitors.
.PARAMETER ComputerName
The Computer name or computer names of remote computers to query.
.EXAMPLE
Get-CCAttachedMonitor -ComputerName Workstation-R01
.NOTES
AUTHOR: Bojan G. Kalicanin
DATE: 26-Jul-2018
VERSION: 1.00
#>
[CmdletBinding()]
param(
    [Parameter(Mandatory=$true,HelpMessage='Enter remote computer name')]
    [Alias('Hostname')]
    [string[]]$ComputerName
)

$f = {
    function ConvertTo-Char($asciiArray) {
        # Convert array of ASCII bytes to the list of characters
        $output = ""
        foreach($c in $asciiArray) {
            $output += [char]$c
        }

        return $output.Trim()
    }
}

Invoke-Command -ComputerName $ComputerName -ArgumentList "$f" -ScriptBlock {
    param($f)
    Invoke-Expression $f
    $attachedMonitors = Get-CimInstance -Namespace root/WMI -ClassName WmiMonitorID
    $results = foreach($monitor in $attachedMonitors) {
        New-Object -TypeName psobject -Property @{
            Active = $monitor.Active
            Manufacturer = ConvertTo-Char($monitor.ManufacturerName)
            UserFriendlyName = ConvertTo-Char($monitor.UserFriendlyName)
            ProductCode = ConvertTo-Char($monitor.ProductCodeID)
            ManufactureDate = "$($monitor.WeekOfManufacture)/$($monitor.YearOfManufacture)"
        }
    }
    $results
}
