<#
.SYNOPSIS
Set-WiFiConnectionProfile sets WiFi profile read from xml config file.
.DESCRIPTION
.PARAMETER ComputerName
Target remote computer name to deploy configuration.
.PARAMETER FilePath
Path to xml configuration file.
.EXAMPLE
Set-WiFiConnectionProfile -ComputerName COMPUTER-R1 -FilePath "$HOME\WiFi-Profile.xml"
.NOTES
AUTHOR: Bojan G. Kalicanin
DATE: 27-Jul-2018
VERSION: 1.00
#>
[CmdletBinding()]
param(
    [Parameter(Mandatory=$true,HelpMessage='Enter remote computer name')]
    [Alias('Hostname')]
    [string[]]$ComputerName='localhost',
    [Parameter(Mandatory=$true,HelpMessage='Enter file path to WiFi profile in xml')]
    [Alias('Path')]
    [string]$FilePath
)
function Add-WiFiConnectionProfile {
    param (
        [Parameter(Mandatory=$true,HelpMessage='Enter path to WiFi profile in xml')]
        [Alias('Path')]
        [string]$FilePath,
        [string]$ComputerName
    )
    Write-Verbose -Message "Deploying WiFi profile from $FilePath to $ComputerName..."
    Invoke-Command -ComputerName $ComputerName -ScriptBlock {
        netsh wlan add profile filename="$FilePath"
    }
    Write-Verbose -Message "Added WiFi profile."
}

Add-WiFiConnectionProfile -ComputerName $ComputerName -FilePath $FilePath