<#
.SYNOPSIS
.DESCRIPTION
.PARAMETER
.EXAMPLE
.NOTES
AUTHOR: Bojan G. Kalicanin
DATE: 27-Jul-2018
VERSION: 1.00
TODO: Implement quering the computer owned by user name, and after that quering to choose if we want to query that computer
for local group that exist or to quit. If we proceed than return local groups, and after that query if we wish to query some of its
groups for the user members. After user members have been returned, ask if we wanna quit or we wanna proceed with removing some of its
members. Note: Utilize ValidateSet parameter attribute.
#>
[CmdletBinding()]
param(
    [string]$User
)
function Get-UserOwnedComputer {
    param(
        [string]$User
    )
    # Implement checking for AD Module
    $result = Get-ADComputer -Filter "Name -like '*'" -Properties Description |
        Where-Object -FilterScript { $_.Description -like "*john.smith*" }

    return $result
}

function Get-RemoteLocalGroupMember {
    param(
        [string]$ComputerName,
        [string]$LocalGroup
    )
    $result = Invoke-Command -ComputerName $ComputerName -ScriptBlock {
        Get-LocalGroupMember -Name $LocalGroup
        }

    return $result.Split()
}

function Remove-RemoteLocalGroupMember {
    param(
        [string]$ComputerName,
        [string]$LocalGroup,
        [string]$Username
    )
    $result = Invoke-Command -ComputerName $ComputerName -ScriptBlock {
        Remove-LocalGroupMember -Group $LocalGroup -Member $Username
    }

    return $result.Split()
}

#Invoke-Command -ScriptBlock { Get-LocalGroupMember -Name Administrators } -ComputerName COMP-R1
#Invoke-Command -ScriptBlock { Remove-LocalGroupMember -Group "Administrators" -Member "DOMAIN\john.smith"} -ComputerName COMP-R1