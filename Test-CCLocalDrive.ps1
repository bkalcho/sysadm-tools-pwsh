<#
.SYNOPSIS
Test-CCLocalDrive tests and repair the local drives from errors.
.DESCRIPTION
Test-CCLocalDrive detects all local drives on computer and performs scanning
without fixing, with just reporting, or so called 'spotfixing' which repairs
drives with little or no downtime.
.PARAMETER Computername
Name of the remote computer to query.
.PARAMETER Mode
Mode of operation, can be either 'scan', which is similar like chkdsk /scan,
or 'fix', which is similar to chkdsk /spotfix.
scan - performs an online scan on the volume.
fix - performs an fix only on reported bad blocks.
.EXAMPLE
Perform just scanning of the all local drives on remote computer:

Test-CCLocalDrive -ComputerName SERVER-R2 -Mode scan
.EXAMPLE
Perform spot fixing on all local drives of remote computer:

Test-CCLocalDrive -ComputerName SERVER-R2 -Mode fix
.NOTES
AUTHOR: Bojan G. Kalicanin
DATE: 26-Jul-2018
VERSION: 1.00
TODO: Implement assigning it as a job and sending report on email if errors found
#>
[CmdletBinding()]
param(
    [Parameter(Mandatory=$true,HelpMessage='Enter remote computer name')]
    [Alias('Hostname')]
    [string[]]$ComputerName,

    [Parameter(Mandatory=$true,HelpMessage='Enter mode of operation')]
    [ValidateSet('scan','fix')]
    [string]$Mode
)

Invoke-Command -ComputerName $ComputerName -ScriptBlock {
    $diskVolumes = (Get-CimInstance -Namespace root/CIMV2 `
        -ClassName Win32_LogicalDisk | Where-Object -Property DriveType `
        -EQ -Value 3).DeviceID.TrimEnd(':')
        foreach($diskVolume in $diskVolumes) {
            if ($Mode -eq 'scan') {
                Repair-Volume -DriveLetter $diskVolume -Scan
            } elseif ($Mode -eq 'fix') {
                Repair-Volume -DriveLetter $diskVolume -SpotFix
            }
        }

}
