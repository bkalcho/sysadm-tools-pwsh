<#
.SYNOPSIS
Get-CCLocalGroupMembership retrieves local group members.
.DESCRIPTION
Get-CCLocalGroupMembership uses WMI to retrieve the Win32_GroupUser instances
from one or more computers. It displays each group user members, and the queried computer.
.PARAMETER ComputerName
The computer name, or names, to query.
NOTE: This cmdlet does not use WSMAN protocol for remoting, it uses native WMI for remote
connections, so it should work even if PS remoting has not been explicitly enabled.
.PARAMETER GroupName
The group name, or names, to query.
.EXAMPLE
Retrieve the members of built-in Administrators group:
Get-CCLocalGroupMembers -ComputerName SERVER-R1 -GroupName Administrators
.EXAMPLE
Retrieve the members of multiple local groups on remote computers:
Get-CCLocalGroupMembers -ComputerName SERVER-R1,SERVER-R2 -GroupName Administrators,Users
.NOTES
AUTHOR: Bojan G. Kalicanin
DATE: 24-Jul-2018
VERSION: 1.00
#>
[CmdletBinding()]
param(
    [Parameter(Mandatory=$true,HelpMessage="Enter name of remote computer")]
    [Alias('hostname')]
    [string[]]$ComputerName,
    [Parameter(Mandatory=$true,HelpMessage="Enter name of the local group")]
    [Alias('localGroup')]
    [string[]]$GroupName
)

foreach ($computer in $ComputerName) {
    foreach ($group in $GroupName) {
        $groupComponent = "\\$computer\root\cimv2:Win32_Group.Domain=`"$computer`",Name=`"$group`""
        Write-Verbose "Quering Group Component $groupComponent"
        Get-WmiObject -ComputerName $computer -Namespace root\cimv2 -Class Win32_GroupUser |
            Where-Object -FilterScript { $_.GroupComponent -eq $groupComponent } |
            ForEach-Object -Process {
                New-Object -TypeName psobject -Property @{
                    'Username' = $_.PartComponent.Split(',')[-1].TrimStart('Name=').Trim('"');
                    'Group' = $group;
                    'ComputerName' = $computer
                } | Select-Object -Property Username,Group,ComputerName
            }
    }
}
